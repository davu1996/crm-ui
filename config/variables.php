<?php
// Variables
return [
  "creatorName" => "SMBPLUS",
  "creatorUrl" => "https://smbplus.vn/",
  "templateName" => "OA CHAT4WORK",
  "templateSuffix" => "Laravel Admin Dashboard Template",
  "templateVersion" => "1.0.0",
  "templateFree" => false,
  "templateDescription" => "Laravel Admin Dashboard Template",
  "templateKeyword" => "Laravel, Bootstrap, Admin Dashboard,Zalo OA",
  "licenseUrl" => "#",
  "livePreview" => "#",
  "productPage" => "#",
  "support" => "info@smbplus.vn",
  "moreThemes" => "#",
  "documentation" => "#",
  "changelog" => "#",
  "repository" => "#",
  "gitRepo" => "#",
  "gitRepoAccess" => "#",
  "githubFreeUrl" => "#",
  "facebookUrl" => "#",
  "twitterUrl" => "#",
  "githubUrl" => "#",
  "dribbbleUrl" => "#",
  "instagramUrl" => "#"
];
