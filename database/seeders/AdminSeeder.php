<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User; // Thay đổi `User` thành model người dùng của bạn

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Tạo tài khoản admin@smb.com
        User::create([
            'name' => 'Admin',
            'email' => 'admin@smb.com',
            'password' => bcrypt('password@123'), // Thay đổi mật khẩu nếu muốn
        ]);

        // Tạo tài khoản superadmin@smb.com
        User::create([
            'name' => 'Super Admin',
            'email' => 'superadmin@smb.com',
            'password' => bcrypt('password@987'), // Thay đổi mật khẩu nếu muốn
        ]);
    }
}