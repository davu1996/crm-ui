<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class FreshworksController extends Controller
{
  public function createContact(Request $request)
  {
    $request->validate([
      'first_name' => 'required|string',
      'last_name' => 'required|string',
      'email' => 'required|email',
      'interested_in' => 'required|string'
    ]);

    $url = 'https://remitano-741106983619957431.myfreshworks.com/crm/sales/api/contacts';
    $token = 'bYPY_yTnsEQLuoI71g1eyw';

    $data = [
      'contact' => [
        'first_name' => $request->first_name,
        'last_name' => $request->last_name,
        'email' => $request->email,
        'custom_field' => [
          'cf_interested_in' => $request->interested_in
        ]
      ]
    ];
    try {
      $response = Http::withHeaders([
        'Authorization' => 'Token token=' . $token,
        'Content-Type' => 'application/json'
      ])->post($url, $data);

      if ($response->successful()) {
        return response()->json([
          'success' => true,
          'message' => 'Contact created successfully',
          'data' => $response->json()
        ], 201);
      } else {
        return response()->json([
          'success' => false,
          'message' => 'Failed to create contact',
          'error' => $response->json()
        ], $response->status());
      }
    } catch (\Exception $e) {
      return response()->json([
        'success' => false,
        'message' => 'An error occurred',
        'error' => $e->getMessage()
      ], 500);
    }
  }
}
