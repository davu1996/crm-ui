
<!-- BEGIN: Vendor JS-->
<script src="{{asset('assets/_vendor/js/dropdown-hover.js')}}"></script>
<script src="{{asset('assets/_vendor/js/mega-dropdown.js')}}"></script>
<script src="{{ asset(mix('assets/_vendor/libs/popper/popper.js')) }}"></script>
<script src="{{ asset(mix('assets/_vendor/js/bootstrap.js')) }}"></script>
<script src="{{ asset(mix('assets/_vendor/libs/node-waves/node-waves.js')) }}"></script>

@yield('vendor-script')
<!-- END: Page Vendor JS-->
<!-- BEGIN: Theme JS-->
<script src="{{ asset(mix('assets/js/front-main.js')) }}"></script>
<!-- END: Theme JS-->
<!-- Pricing Modal JS-->
@stack('pricing-script')
<!-- END: Pricing Modal JS-->
<!-- BEGIN: Page JS-->
@yield('page-script')
<!-- END: Page JS-->
