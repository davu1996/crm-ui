<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Http;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
  return $request->user();
});


Route::get('/contacts', function (Request $request) {
  $request->validate([
    'first_name' => 'required|string',
    'last_name' => 'required|string',
    'email' => 'required|email',
    'interested_in' => 'required|string',
  ]);

  // URLs and Tokens
  $contactUrl = 'https://remitano-743711297498603115.myfreshworks.com/crm/sales/api/contacts';
  $eventUrl = 'https://remitano-743711297498603115.myfreshworks.com/crm/marketer/mas/api/v1/events';
  $token = 'wljDzcyokVFOLacz1SgLmw';

  // Prepare data for the contact creation
  $contactData = [
    'contact' => [
      'first_name' => $request->first_name,
      'last_name' => $request->last_name,
      'email' => $request->email,
    ]
  ];

  // Prepare data for the event registration
  $eventData = [
    'product' => $request->interested_in // Hardcoded as per your requirement
  ];

  try {
    // First API request to create the contact
    $contactResponse = Http::withHeaders([
      'Authorization' => 'Token token=' . $token,
      'Content-Type' => 'application/json'
    ])->post($contactUrl, $contactData);

    if ($contactResponse->successful()) {
      // Second API request to register the event with the product "Bitcoin"
      $eventResponse = Http::withHeaders([
        'Authorization' => 'Token token=' . $token,
        'Content-Type' => 'application/json'
      ])->post($eventUrl . '?name=' . urlencode($request->first_name) . '&email=' . urlencode($request->email) . '&event_name=Interested%20In', $eventData);

      if ($eventResponse->successful()) {
        return response()->json([
          'success' => true,
          'message' => 'Contact created and event registered successfully',
          'contact_data' => $contactResponse->json(),
          'event_data' => $eventResponse->json()
        ], 201);
      } else {
        return response()->json([
          'success' => false,
          'message' => 'Contact created but failed to register event',
          'error' => $eventResponse->json()
        ], $eventResponse->status());
      }
    } else {
      return response()->json([
        'success' => false,
        'message' => 'Failed to create contact',
        'error' => $contactResponse->json()
      ], $contactResponse->status());
    }
  } catch (\Exception $e) {
    return response()->json([
      'success' => false,
      'message' => 'An error occurred',
      'error' => $e->getMessage()
    ], 500);
  }
})->name('createContact');
